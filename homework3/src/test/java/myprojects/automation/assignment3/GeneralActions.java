package myprojects.automation.assignment3;

import org.openqa.selenium.*;
import myprojects.automation.assignment3.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import java.util.Random;


/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    public WebDriver driver;
    private WebDriverWait wait;
    public String categoryName;
    public String login ="webinar.test@gmail.com";
    public String password ="Xcg7299bnSmMuRLp9ITw";


    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);

    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */

     public void login(String login, String password) {
           String AdminPanel = Properties.getBaseAdminUrl();
                for (int i=0; i<=3; i++) {
                     try{
                        driver.findElement(By.id("email")).sendKeys(login);
                        driver.findElement(By.id("passwd")).sendKeys(password);
                        driver.findElement(By.name("submitLogin")).click();
                         }
                     catch( NoSuchElementException e){}
                }

          waitForContentLoad();

         throw new UnsupportedOperationException();
    }

    /**
     * Adds new category in Admin Panel.
     * //@param categoryName
     */


    public static String generate() {
        Random random = new Random();
        String categoryName = random.toString();
        return categoryName;
    }

    public void createCategory(String categoryName) {
            Actions builder = new Actions(driver);
            builder.moveToElement(driver.findElement(By.xpath("//li[@data-submenu='9']")));

                            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("subtab-AdminCategories")));

            builder.moveToElement(driver.findElement(By.id("subtab-AdminCategories"))).click().build().perform();

                            wait.until(ExpectedConditions.presenceOfElementLocated
                                    (By.xpath("//body[@class='ps_back-office page-sidebar admincategories']")));

            driver.findElement(By.className("process-icon-new")).click();

                            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("category_form")));

            driver.findElement(By.xpath("//input[@id='name_1']")).sendKeys(generate());


            ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();",
                    driver.findElement(By.xpath("//button[@id='category_form_submit_btn']")));


            driver.findElement(By.xpath("//button[@id='category_form_submit_btn']")).click();

                           if (driver.findElement(By.xpath
                           ("//div[@class='alert alert-success']")).isDisplayed())
                                     {System.out.println("Новая категория сохранена");}

        throw new UnsupportedOperationException();

    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad()
    {
        new WebDriverWait(driver, 50).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }

}





